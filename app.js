const   request = require("request-promise"),
        express = require("express"),
        bodyParser = require('body-parser'),
        app = express(),
        db = require("./db"),
        JSON = require("circular-json");
db.connect();

app.use(bodyParser.json({strict : false}));

app.listen(3000, function(){
    console.log("Example app listening on port 3000!");
});

app.get("/ping", (req, res) => {
    console.log("pong");
    res.send("pong");
});

app.all("/v1/:id", async (req, res) => {
    let exist = 0,
        data = null;
    await db.get().exists(req.params.id, (err, items) => {
        exist = items;
    });
    if (exist === 1){
        await db.get().hgetall(req.params.id, (err, items) => {
            data = items;
        } );
    } else {
        console.log("No such item");
        res.sendStatus(404);
    }
    console.log(data.port, data.redirect);
    let uri = `http://localhost:${data.port}${data.redirect}`,
        options = {
            uri: uri,
            method: `${req.method}`,
            body: `${JSON.stringify({req})}`,
            json: true
        };
    request(options)
        .then( response => {
            console.log(response);
            res.sendStatus(200);
        })
        .catch( err => {
            console.log(err);
            res.sendStatus(400);
        });
});