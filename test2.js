const   request = require("request-promise"),
    body_parser= require('body-parser')
express = require("express"),
    app = express(),
    db = require("./db"),
    async = require("async");

db.connect();

db.get().hgetall("test2", (err, items) => {
    let data = items;
    app.listen(data["port"], () =>{
        console.log(`Test2 app listening on port ${data["port"]}`);
    })
} );

// app.use(body_parser.urlencoded({extended : true}));
app.use(body_parser.json({strict : false}));

app.post("/test2", (req, res) =>{
    console.log("POST in TEST2");
    let original = JSON.parse(req.body).req;
    // console.log(Object.keys(JSON.parse(req.body).req));
    console.log(original.body);
    res.sendStatus(200);
});

app.get("/test2", (req, res) =>{
    console.log("GET");
    console.log(JSON.parse(req.body));
    res.sendStatus(200);
});
